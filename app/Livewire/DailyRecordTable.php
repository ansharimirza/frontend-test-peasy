<?php

namespace App\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class DailyRecordTable extends Component
{

    public $showTable = false;
    public $dailyRecords;
    public $page = 1;
    public $hasMore;
    public $currentPage;
    public $totalData;

    protected $listeners = ['switchTables','refreshDailyRecords'];

    public function switchTables()
    {
        $this->showTable = !$this->showTable;
    }

    public function refreshDailyRecords()
    {
        $this->getDailyRecords();
    }

    public function mount()
    {
        $this->getDailyRecords();
    }

    public function getDailyRecords()
    {
        // Panggil API untuk mendapatkan data pengguna dengan pagination
        $response = Http::get(env('URL'). '/api/daily-record', [
        ]);

        // Periksa status respons
        if ($response->successful()) {
            // Dapatkan data dalam bentuk array
            $responseData = $response->json();
            // $this->dailyRecords = $response->json()['data'];

            // Periksa apakah ada data pengguna dalam respons
            if (isset($responseData['data'])) {
                // Ubah format nama dan lokasi pengguna dari JSON menjadi array
                foreach ($responseData['data'] as &$daily) {
                    // Ubah format tanggal
                    $daily['date'] = \Carbon\Carbon::parse($daily['date'])->format('m/d/Y');

                    $daily['male_avg_age'] = round($daily['male_avg_age'], 2);
                    $daily['female_avg_age'] = round($daily['female_avg_age'], 2);
                }

                // Set nilai properti tambahan
                $this->dailyRecords = $responseData['data'];
                $this->currentPage = $responseData['pagination']['current_page'];
                $this->hasMore = $responseData['pagination']['has_more'];
                $this->totalData = $responseData['pagination']['total'];
            } else {
                // Jika tidak ada data pengguna dalam respons, kembalikan array kosong
                $this->dailyRecords = [];
            }
        } else {
            // Jika tidak berhasil, tampilkan pesan kesalahan atau kembalikan array kosong
            $this->dailyRecords = [];
        }
    }

    public function render()
    {
        return view('livewire.daily-record-table');
    }
}

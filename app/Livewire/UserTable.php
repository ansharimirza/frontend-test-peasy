<?php

namespace App\Livewire;

use Carbon\Carbon;
use Livewire\WithPagination;
use Livewire\Component;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UserTable extends Component
{
    use WithPagination;

    public $showTable = true;
    public $users;
    public $page = 1;
    public $hasMore;
    public $currentPage;
    public $totalData;
    public $search = '';

    protected $listeners = ['switchTables'];

    public function switchTables()
    {
        $this->showTable = !$this->showTable;
    }

    public function mount()
    {
        // Memanggil method getUsers() saat komponen di-mount
        $this->getUsers();
    }

    public function gotoPage($page)
    {
        $this->page = $page;
        $this->getUsers();
    }


    public function getUsers()
    {
        // Panggil API untuk mendapatkan data pengguna dengan pagination
        $response = Http::get(env('URL'). '/api/user/', [
            'page' => $this->page, // Menggunakan halaman saat ini yang diberikan oleh Livewire
            'search' => $this->search, // Mengirim parameter pencarian ke API
        ]);

        // Periksa status respons
        if ($response->successful()) {
            // Dapatkan data dalam bentuk array
            $responseData = $response->json();

            // Periksa apakah ada data pengguna dalam respons
            if (isset($responseData['data'])) {
                // Ubah format nama dan lokasi pengguna dari JSON menjadi array
                foreach ($responseData['data'] as &$user) {
                    $user['name'] = json_decode($user['name'], true);

                    // Ubah format tanggal
                    $user['created_at'] = \Carbon\Carbon::parse($user['created_at'])->format('m/d/Y');
                }

                // Set nilai properti tambahan
                $this->users = $responseData['data'];
                $this->currentPage = $responseData['pagination']['current_page'];
                $this->hasMore = $responseData['pagination']['has_more'];
                $this->totalData = $responseData['pagination']['total'];
                // return $responseData['data'];
            } else {
                // Jika tidak ada data pengguna dalam respons, kembalikan array kosong
                $this->users = [];
            }
        } else {
            // Jika tidak berhasil, tampilkan pesan kesalahan atau kembalikan array kosong
            $this->users = [];
        }
    }

    // Metode untuk melakukan pencarian saat tombol pencarian ditekan
    public function searchUsers()
    {
        $this->page = 1; // Reset halaman ke halaman pertama saat pencarian baru dimulai
        $this->getUsers(); // Panggil metode getUsers untuk mendapatkan data dengan pencarian baru
    }

    public function deleteUser($uuid)
    {
        $response = Http::delete(env('URL'). '/api/user/'.$uuid);

        if ($response->successful()) {
            // Jika penghapusan berhasil, perbarui data pengguna
            $this->getUsers();
            $this->dispatch('refreshDailyRecords');
        }
    }


    public function render()
    {
        return view('livewire.user-table');
    }
}

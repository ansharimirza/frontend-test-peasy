<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Backend Test</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    @livewireStyles
    <style>
        /* CSS kustom dapat ditambahkan di sini */
    </style>
</head>
<body>

<div class="container mt-5">
<div class="row mt-3">
        <div class="col">
        <button id="switchButton" class="btn btn-primary mt-3">Switch Tables</button>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div id="userTable">
                @livewire('user-table')
            </div>
            <div id="displayRecordTable" style="display: none;">
                @livewire('daily-record-table')
            </div>
        </div>
    </div>

</div>

@livewireScripts
<script>
    document.getElementById('switchButton').addEventListener('click', function () {
        var userTable = document.getElementById('userTable');
        var displayRecordTable = document.getElementById('displayRecordTable');

        if (userTable.style.display === 'block') {
            userTable.style.display = 'none';
            displayRecordTable.style.display = 'block';
        } else {
            userTable.style.display = 'block';
            displayRecordTable.style.display = 'none';
        }
    });

    document.addEventListener('livewire:load', function () {
        Livewire.hook('message.sent', (message, component) => {
            // Tangkap event 'refreshDailyRecords' dari komponen UserTable
            if (message.type === 'refreshDailyRecords') {
                window.livewire.emit('refreshDailyRecords');
            }
        });
    });

</script>

</body>
</html>

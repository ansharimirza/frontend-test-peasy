<div class="container mt-5">
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Male Avg Count</th>
                <th>Female Avg Count</th>
                <th>Male Avg Age</th>
                <th>Female Avg Age</th>
            </tr>
        </thead>
        <tbody>
            @foreach($dailyRecords as $record)
            <tr>
                <td>{{ $record['date'] }}</td>
                <td>{{ $record['male_count'] }}</td>
                <td>{{ $record['female_count'] }}</td>
                <td>{{ $record['male_avg_age'] }}</td>
                <td>{{ $record['female_avg_age'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="text-center">
        Showing page {{ $currentPage }}
    </div>
    <div class="text-center">
        Showing total {{ $totalData }} Data
    </div>
    <div class="text-center">
        @if ($currentPage > 1)
            <a href="#" class="btn btn-primary" wire:click="gotoPage({{ $currentPage - 1 }})">Previous</a>
        @endif

        @if ($hasMore)
            <a href="#" class="btn btn-primary" wire:click="gotoPage({{ $currentPage + 1 }})">Next</a>
        @endif
    </div>
</div>

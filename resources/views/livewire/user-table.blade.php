<div class="container mt-5">
    <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <input type="text" class="form-control" wire:model="search" placeholder="Search...">
                <button class="btn btn-outline-secondary" type="button" wire:click="searchUsers">Search</button>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user['name']['first'] }} {{ $user['name']['last'] }}</td>
                        <td>{{ $user['age'] }}</td>
                        <td>{{ $user['gender'] }}</td>
                        <td>{{ $user['created_at'] }}</td>
                        <td>
                            <button class="btn btn-danger" wire:click="deleteUser('{{ $user['uuid'] }}')">Delete</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                Showing page {{ $currentPage }}
            </div>
            <div class="text-center">
                Showing total {{ $totalData }} users
            </div>
            <div class="text-center">
                @if ($currentPage > 1)
                    <a href="#" class="btn btn-primary" wire:click="gotoPage({{ $currentPage - 1 }})">Previous</a>
                @endif

                @if ($hasMore)
                    <a href="#" class="btn btn-primary" wire:click="gotoPage({{ $currentPage + 1 }})">Next</a>
                @endif
            </div>
        </div>
    </div>
</div>
